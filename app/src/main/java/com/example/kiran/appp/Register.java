package com.example.kiran.appp;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.kiran.appp.model.Admin;
import com.example.kiran.appp.util.HttpManager;

import com.google.gson.Gson;

public class Register extends AppCompatActivity {
    EditText name,email,password,phone;
    String Fname,Femail,Fpassword,Fphone;
    Button b;
    Admin ad=new Admin();
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        b=findViewById(R.id.Rbtn);
        name=findViewById(R.id.Rname);
        email=findViewById(R.id.Remail);
        password=findViewById(R.id.Rpass);
        phone=findViewById(R.id.Rphno);




        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v1) {
                Fname=name.getText().toString();
                Femail=email.getText().toString();
                Fpassword=password.getText().toString();
                Fphone=phone.getText().toString();

                System.out.println(Fname+Fphone);

                ad.setAname(Fname);
                ad.setApass(Fpassword);
                ad.setAphno(Fphone);
                ad.setAemail(Femail);
                RegisterUser registerUser=new RegisterUser();
                registerUser.execute();
            }
        });



    }
    class RegisterUser extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... params) {
            HttpManager httpManager = new HttpManager(getApplicationContext());
            Gson gson = new Gson();
            String  userJson = gson.toJson(ad,Admin.class);
            System.out.println("User Json - "+userJson);
            String result = httpManager.postData(getResources().getString(R.string.serviceUrl)+"userService/addAdmin", userJson);
            System.out.println("Result - "+result);

            return result;
        }
        protected void onPostExecute(String result) {
            System.out.println("Result - "+result);
            if(result.equals("1")) {
                System.out.println("Result - "+result);
                Intent myIntent = new Intent(Register.this, Login.class);
                startActivity(myIntent);
            }


            if(result.equals("-1")){
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                TextView myMsg = new TextView(context);

                builder1.setMessage("PHONE NUMBER ALREADY EXSISTS");
                builder1.setCancelable(true);

                builder1.setPositiveButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        }
    }
}
