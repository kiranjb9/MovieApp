package com.example.kiran.appp;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.kiran.appp.model.Admin;
import com.example.kiran.appp.util.HttpManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class Login extends AppCompatActivity {

    Button b;
    TextView signup;
    EditText phno,etUserpass;
    String result;
    Context context;
    Admin ad=new Admin();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        b=findViewById(R.id.buttonID);
        phno = findViewById(R.id.phno);
        etUserpass =  findViewById(R.id.p);

        //sign up
        signup=findViewById(R.id.link_signup);
        signup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v1) {
                Intent launchActivity1= new Intent(Login.this,Register.class);
                startActivity(launchActivity1);

            }
        });




        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String strUserpass = etUserpass.getText().toString();
                final String ph = phno.getText().toString();
                System.out.println("data from edit box"+ph+strUserpass);
                ad.setAphno(ph);
                ad.setApass(strUserpass);
                RegisterUser registerUser=new RegisterUser();
                registerUser.execute();


            }
        });

    }
    class RegisterUser extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... params) {
            HttpManager httpManager = new HttpManager(getApplicationContext());
            Gson gson = new Gson();

            String  userJson = gson.toJson(ad,Admin.class);
            System.out.println("User Json - "+userJson);
            result = HttpManager.postData(getResources().getString(R.string.serviceUrl)+"userService/login", userJson);

            return result;
        }


        protected void onPostExecute(String result) {
            System.out.println("Result - "+result);
            Gson gson = new Gson();
            final List<Admin> userList = gson.fromJson(result,new TypeToken<List<Admin>>()
            {
            }.getType());

            if(TextUtils.isEmpty(result)){

            }
            else {


                if(userList!=null && userList.size()>0){
                    Intent myIntent = new Intent(Login.this, homepage.class);
                    Bundle b = new Bundle();
                    System.out.println(userList.get(0).toString());
                    //Inserts a String value into the mapping of this Bundle
                    b.putString("name", userList.get(0).getAname().toString());
                    b.putString("phno", userList.get(0).getAphno().toString());
                    b.putString("email", userList.get(0).getAemail().toString());
                    b.putString("pass", userList.get(0).getApass().toString());


                    myIntent.putExtras(b);
                    startActivityForResult(myIntent, 0);

                }

        }
    }

}

}
