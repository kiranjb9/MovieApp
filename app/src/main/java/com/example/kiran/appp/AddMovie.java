package com.example.kiran.appp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.kiran.appp.model.Admin;
import com.example.kiran.appp.model.Movie;
import com.example.kiran.appp.util.HttpManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Fragment3.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Fragment3#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddMovie extends Fragment {
    EditText mname,mlanguage,mgenre,url;
    Button addMovie;
    String result;
    Context context;
    Movie movie=new Movie();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AddMovie() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment3.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment3 newInstance(String param1, String param2) {
        Fragment3 fragment = new Fragment3();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.add_movie, container, false);

        mname=view.findViewById(R.id.Mname);
        mlanguage=view.findViewById(R.id.Mlanguage);
        mgenre=view.findViewById(R.id.Mgenre);
        url=view.findViewById(R.id.mURL);

        addMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String name = mname.getText().toString();

                final String lan = mlanguage.getText().toString();
                final String genre = mgenre.getText().toString();
                final String Movieurl = url.getText().toString();

                Bundle bundle = getActivity().getIntent().getExtras();

                movie.setAd(bundle.getString("phno"));
                System.out.println(bundle.getString("phno"));
                movie.setMname(name);
                movie.setLanguage(lan);
                movie.setGenre(genre);
                movie.setUrl(Movieurl);


                RegisterUser registerUser=new RegisterUser();
                registerUser.execute();

            }
        });




        if (mListener != null) {
            mListener.onFragmentInteraction("ADD MOVIES");
        }



        return view;

    }





    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title);
    }

    class RegisterUser extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... params) {
            HttpManager httpManager = new HttpManager(context);
            Gson gson = new Gson();

            String  userJson = gson.toJson(movie,Movie.class);
            System.out.println("User Json - "+userJson);
            result = HttpManager.postData(getResources().getString(R.string.serviceUrl)+"userService/AddMovie", userJson);

            return result;
        }


        protected void onPostExecute(String result) {
            System.out.println("Result - "+result);
            Gson gson = new Gson();
            final List<Admin> userList = gson.fromJson(result,new TypeToken<List<Admin>>()
            {
            }.getType());

            if(TextUtils.isEmpty(result)){

            }
            else {


                if(userList!=null && userList.size()>0){

                    Bundle b = new Bundle();
                    System.out.println(userList.get(0).toString());
                    //Inserts a String value into the mapping of this Bundle
                    b.putString("name", userList.get(0).getAname().toString());
                    b.putString("phno", userList.get(0).getAphno().toString());
                    b.putString("email", userList.get(0).getAemail().toString());
                    b.putString("pass", userList.get(0).getApass().toString());



                }

            }
        }

    }
}
