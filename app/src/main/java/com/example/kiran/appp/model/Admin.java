package com.example.kiran.appp.model;

import java.util.ArrayList;
import java.util.List;

public class Admin {

    private String aphno;
    private String aname;
    private String aemail;
    private String apass;
    private List<Movie> m=new ArrayList<Movie>();

    public String getAphno() {
        return aphno;
    }

    public void setAphno(String aphno) {
        this.aphno = aphno;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getAemail() {
        return aemail;
    }

    public void setAemail(String aemail) {
        this.aemail = aemail;
    }

    public String getApass() {
        return apass;
    }

    public void setApass(String apass) {
        this.apass = apass;
    }

    public List<Movie> getM() {
        return m;
    }

    public void setM(List<Movie> m) {
        this.m = m;
    }
}
